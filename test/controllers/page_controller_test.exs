defmodule SmartH0me.PageControllerTest do
  use SmartH0me.ConnCase

  test "GET /" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end
end
