defmodule SmartH0me do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the endpoint when the application starts
      supervisor(SmartH0me.Endpoint, []),
      # Start the Ecto repository
      worker(SmartH0me.Repo, []),
      # Here you could define other workers and supervisors as children
      # worker(SmartH0me.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SmartH0me.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SmartH0me.Endpoint.config_change(changed, removed)
    :ok
  end
end
