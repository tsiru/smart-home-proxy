use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :smart_h0me, SmartH0me.Endpoint,
  secret_key_base: "XXlpSjqxVKIa+jTogXOSSqb563Ur27suiWF9TpueVBy20IK/rAMjfxXHil96apjg"

# Configure your database
config :smart_h0me, SmartH0me.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "smart_h0me_prod",
  size: 20 # The amount of database connections in the pool
