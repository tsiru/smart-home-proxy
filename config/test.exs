use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :smart_h0me, SmartH0me.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :smart_h0me, SmartH0me.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "smart_h0me_test",
  size: 1 # Use a single connection for transactional tests
