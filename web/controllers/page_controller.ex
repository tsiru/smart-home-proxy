defmodule SmartH0me.PageController do
  use SmartH0me.Web, :controller

  plug :action

  def poll(conn, _params) do
    conn
    |> put_resp_content_type("text/html")
    |> render "poll.json"
  end

  def on(conn, _params) do
    {:ok, file} = File.open "hello", [:write]
    IO.write file, 'on'
    File.close file

    conn
    |> put_resp_content_type("text/html")
    |> render "poll.json"
  end

  def off(conn, _params) do
    {:ok, file} = File.open "hello", [:write]
    IO.write file, 'off'
    File.close file

    conn
    |> put_resp_content_type("text/html")
    |> render "poll.json"
  end
end
