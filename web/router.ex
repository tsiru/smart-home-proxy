defmodule SmartH0me.Router do
  use SmartH0me.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug CORSPlug, [origin: "*"]
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SmartH0me do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :poll
    get "/on", PageController, :on
    get "/off", PageController, :off
  end

  # Other scopes may use custom stacks.
  # scope "/api", SmartH0me do
  #   pipe_through :api
  # end
end
